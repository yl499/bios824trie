
# coding: utf-8

# In[467]:


import glob
import numpy as np


# In[468]:


def readfiles(reads_dir = '/Users/yingzhouliu/Desktop/824_2/bios824-notebook-spring2019/my.gen.fastq'
              , references_dir = '/Users/yingzhouliu/Desktop/824_2/bios824-notebook-spring2019/my.gen.fasta'):
    """
    Get all reads and all references
    """
    with open(reads_dir) as f:
        # remove whitespace characters like `\n` at the end of each line
        reads = [x.strip() for x in f.readlines()] 
    with open(references_dir) as ff:
        references = [y.strip() for y in ff.readlines()] 
    
    return [reads[i] for i in range(0, len(reads), 4)], [reads[i] for i in range(1, len(reads), 4)], [reads[i] for i in range(2, len(reads), 4)], [reads[i] for i in range(3, len(reads), 4)], [references[j].replace(">", "") for j in range(0, len(references), 2)], [references[j] for j in range(1, len(references), 2)],


# In[469]:


reads_name, reads_seq, reads_idf, reads_score, references_name, references_seq = readfiles()


# In[470]:


def phred_score(base):
    """
    Get phred score from ASCII
    """
    return(ord(base) - 33)

def p_error(score):
    """
    Get p error from score
    """
    return(10**(-score/10))

def joint_probs(read, ref, score):
    """
    Calculate joint probabilities
    """
    L = len(ref)
    prob = []
    for i in range(L):
        if(read[i] == ref[i]):
            prob.append(1 - p_error(phred_score(score[i])))
        else:
            prob.append(p_error(phred_score(score[i]))/3)
    return np.prod(prob)


# In[471]:


from typing import Tuple


class TrieNode(object):
    """
    Our trie node implementation. Very basic. but does the job
    """
    
    def __init__(self, char: str):
        self.char = char
        self.children = []
        # Is it the last base of the sequence
        self.seq_finished = False
        # How many times this sequence appeared in the addition process
        self.counter = 1


# In[472]:


def add_children(root, children: list):
    """
    Adding children in the trie structure
    """
    node = root
    seq = []
    for i, child in enumerate(children):
        if child.char in seq:
            idx = seq.index(child.char)
            children[idx].children.append(child.children)
            seq.append(child.char)
        else:
            seq.append(child.char)
            node.append(child.children)
    return node


# In[473]:


def add(root, seq: str):
    """
    Adding a sequence in the trie structure
    """
    node = root
    for char in seq:
        found_in_child = False
        # Search for the sequence in the children of the present `node`
        for child in node.children:
            if child.char == char:
                # We found it, increase the counter by 1 to keep track that another
                child.counter += 1
                # And point the node to the child that contains this sequence
                node = child
                found_in_child = True
                break
        # We did not find it so add a new chlid
        if not found_in_child:
            new_node = TrieNode(char)
            node.children.append(new_node)
            # And then point node to the new child
            node = new_node
    # Everything finished. Mark it as the end of a word.
    node.seq_finished = True


# In[474]:


reference_tree = TrieNode('*')
for i in range(len(references_seq)):
    add(reference_tree, references_seq[i])


# In[511]:


def find_read(root, read: str, mismatch = 2, L = 18) -> Tuple[bool, str, str]:
    """
    Check and return 
      1. If the read sequence exists in any of the reference sequences
      2. Allow mismatches
    """
    node = root
    # If the root node has no children, then return False.
    # Because it means we are trying to search in an empty trie
    if not root.children:
        return False, 0
    # count the number of mismatch
    mis = 0
    # get reference
    ref = ""
    
    for i, char in enumerate(read):
        
        char_not_found = True
        # Search through all the children of the present `node`
        for child in node.children:
            if child.char == char:
                # We found the char existing in the child within mismatch tolerance.
                char_not_found = False
                # Assign node as the child containing the char and break
                node = child
                # get the reference sequence
                ref+=child.char
                break
        # Return False anyway when we did not find a char.
        if char_not_found and mis <= mismatch:
            # add mismatch
            mis+=1
            # build a new tree
            node_new = TrieNode('*')
            reff = ref.split('*')[0]
            references = [l for l in references_seq if l.startswith(reff)]
            for j in range(len(references)):
                add(node_new, references[j][i+1:L])
            # assign new tree to node    
            node = node_new
            ref+='*'
            
        if mis > mismatch:
            return False, ref, read
    # Well, we are here means we have found the sequence. Return true to indicate that
    return True, ref, read



# In[518]:


np.unique([find_read(reference_tree, read, mismatch=0)[0] for read in reads_seq], return_counts=True)

